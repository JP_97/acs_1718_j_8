package modal;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import utils.DoublyLinkedList;
import utils.DoublyLinkedListUtils;
import utils.Pair;

public class GestorAtendimentoServicosPublicos {
    
    //Tempo de abertura das reparticoes
    public static final double TEMPO_ABERTURA = 9.5;
    //Tempo do fecho das reparticoes
    public static final double TEMPO_FECHO = 15.5;
    //Tempo medio de atendimento em minutos
    public static final int TEMPO_MEDIO_ATENDIMENTO = 10;
    
    private DoublyLinkedList<Reparticao> reparticoes;
    private DoublyLinkedList<Cidadao> cidadaos;
    private DoublyLinkedList<Senha> senhas;

    public GestorAtendimentoServicosPublicos() {
        this.reparticoes = new DoublyLinkedList<>();
        this.cidadaos = new DoublyLinkedList<>();
        this.senhas = new DoublyLinkedList<>();
    }

    public DoublyLinkedList<Reparticao> getReparticoes() {
        return reparticoes;
    }

    public DoublyLinkedList<Cidadao> getCidadaos() {
        return cidadaos;
    }

    public DoublyLinkedList<Senha> getSenhas() {
        return senhas;
    }

    /**
     *
     * Lê as reparticoes de um ficheiro e guarda-as na lista de reparticoes
     *
     * @param ficheiro - nome do ficheiro
     * @throws Exception - encontrando uma linha invalida para a leitura e lança
     * uma excepção
     */
    public void lerReparticoesDeFicheiro(String ficheiro) throws Exception {
        Scanner sc = new Scanner(new File(ficheiro));
        while (sc.hasNext()) {
            String[] line = sc.nextLine().split(",");
            try{
                if (line.length < 3) {
                    throw new Exception("Erro a ler reparticoes do ficheiro: " + ficheiro);
                }
                Reparticao r = new Reparticao(line[0], line[2], Integer.parseInt(line[1]));
                for (int i = 3; i < line.length; i++) {
                    r.addServico(line[i].charAt(0));
                }
                addReparticao(r);
            }catch(Exception e){
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     *
     * Lê os atendimentos presenciais de um ficheiro e guarda-os na lista de
     * cidadaos
     *
     * @param ficheiro - nome do ficheiro
     * @throws Exception - encontrando uma linha invalida para a leitura e lança
     * uma excepção
     */
    public void lerSenhasDeFicheiro(String ficheiro) throws Exception {
        Scanner sc = new Scanner(new File(ficheiro));
        while (sc.hasNext()) {
            String[] line = sc.nextLine().split(",");
            try{
                if (line.length != 3) {
                    throw new Exception("Erro a ler atendimentos do ficheiro: linha inválida");
                }
                Senha s = new Senha(Integer.parseInt(line[2]), line[1].charAt(0), line[0]);
                addSenha(s);
            }catch(Exception e){
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     *
     * Lê os cidadaos de um ficheiro e guarda-os na lista de cidadaos
     *
     * @param ficheiro - nome do ficheiro
     * @throws Exception - encontrando uma linha invalida para a leitura e lança
     * uma excepção
     */
    public void lerCidadaosDeFicheiro(String ficheiro) throws Exception {
        Scanner sc = new Scanner(new File(ficheiro));
        while (sc.hasNext()) {
            String[] line = sc.nextLine().split(",");
            try{
                if (line.length != 5) {
                    throw new Exception("Erro a ler cidadaos do ficheiro: " + ficheiro);
                }
                Cidadao c = new Cidadao(line[1], line[0], line[2], line[3], Integer.parseInt(line[4]));
                addCidadao(c);
            }catch(Exception e){
                System.err.println(e.getMessage());
            }
        }
    }

    public void addReparticao(Reparticao r) throws Exception {
        for (Reparticao r2 : reparticoes) {
            if (r2.getCodigoPostal().equals(r.getCodigoPostal())) {
                throw new Exception("Erro ao adicionar reparticao numero "+ r.getNumeroReparticao() +" : Codigo postal repetido");
            }
        }

        for (Cidadao c : cidadaos) {
            if (c.getCodigoPostal().split("-")[0].equals(r.getCodigoPostal())) {
                c.setNumeroReparticao(r.getNumeroReparticao());
            }
        }

        reparticoes.addLast(r);
    }

    public void addCidadao(Cidadao c) throws Exception {
        if(getReparticaoByNumeroReparticao(c.getNumeroReparticao()) == null){
                throw new Exception("Erro ao adicionar cidadao (NIF"+ c.getNumeroContribuinte() +"): numero de reparticao("+ c.getNumeroReparticao() +") nao associado a nenhuma reparticao");
        }
        for (Cidadao c2 : cidadaos) {
            if (c2.equals(c)) {
                throw new Exception("Erro ao adicionar cidadao (NIF"+ c.getNumeroContribuinte() +"): Cidadao já existe no sistema");
            }
        }
        cidadaos.addLast(c);
    }

    public void addSenha(Senha s) throws Exception {
        Cidadao c = getCidadaoByNumeroContribuinte(s.getNumeroContribuinte());
        if(c == null)
                throw new Exception("Erro ao adicionar senha("+ s.getCodigo() + s.getNumero() +"): Numero de Contribuinte nao associado a nenhum cidadao");
        
        if(!getReparticaoDeCidadao(c).contemServico(s.getCodigo()))
                throw new Exception("Erro ao adicionar senha("+ s.getCodigo() + s.getNumero() +"): Reparticao associada a senha não contem o servico("+ s.getCodigo() +")");
        
        senhas.addLast(s);
    }
    
    public Map<Pair<Integer, String>, List<String>> getCidadaosAfectosACadaReparticao() {
        Map<Pair<Integer, String>, List<String>> m = new HashMap<>();
        for (Reparticao r : reparticoes) {
            if (!m.containsKey(r)) {
                m.put(new Pair<>(r.getNumeroReparticao(), r.getCidade()), new ArrayList<>());
            }
        }
        for (Cidadao c : cidadaos) {
            Reparticao r = getReparticaoDeCidadao(c);
            
            getListaCidadaosByNumeroReparticaoCidade(m,r.getNumeroReparticao(),r.getCidade()).add(c.getNumeroContribuinte());
        }
        return m;
    }
    
    private List<String> getListaCidadaosByNumeroReparticaoCidade(Map<Pair<Integer, String>, List<String>> m, int numeroReparticao, String cidade){
        for(Pair<Integer,String> p : m.keySet()){
            if(p.getFirst() == numeroReparticao && p.getSecond().equals(cidade))
                return m.get(p);
        }
        return null;
    }

    private Reparticao getReparticaoDeCidadao(Cidadao c) {
        for (Reparticao r : reparticoes) {
            if (c.getNumeroReparticao() == r.getNumeroReparticao()) {
                return r;
            }
        }
        return null;
    }

    public boolean removeReparticao(Reparticao r) {
        DoublyLinkedListUtils<Reparticao> listUtils = new DoublyLinkedListUtils<>();
        boolean out = false;
        int numeroReparticao = r.getNumeroReparticao();
        for (Cidadao c : cidadaos) {
            if (c.getNumeroReparticao() == numeroReparticao) {
                out = listUtils.remove(reparticoes, r);
                c.setNumeroReparticao(getNumeroReparticaoMaisProximaCidadao(c.getCodigoPostal()));
            }
        }
        return out;
    }

    private int getNumeroReparticaoMaisProximaCidadao(String codigoPostal) {
        if (reparticoes.size() == 0) {
            return -1;
        }
        String cdg = codigoPostal.split("-")[0];
        int numeroReparticao = reparticoes.first().getNumeroReparticao(),
                distancia = Math.abs(Integer.parseInt(reparticoes.first().getCodigoPostal()) - Integer.parseInt(cdg));
        for (Reparticao r : reparticoes) {
            if (Math.abs(Integer.parseInt(r.getCodigoPostal()) - Integer.parseInt(cdg)) < distancia) {
                numeroReparticao = r.getNumeroReparticao();
            }
        }
        return numeroReparticao;
    }

    private boolean isNextInLine(Senha s,DoublyLinkedList<Senha> l){
        int min = s.getNumero();
        for(Senha senha : l){
            if(senha.getCodigo() == s.getCodigo() && senha.getNumero() < s.getNumero())
                return false;
        }
        return true;
    }
    
    public Map<String,HashMap<Character,String>> getUtilizacaoReparticao(Reparticao r) {
          
          Map<Integer,HashMap<Character,String>> out = new HashMap<>();
          DoublyLinkedListUtils<Senha> lu = new DoublyLinkedListUtils<>();
          DoublyLinkedList<Senha> aux = lu.getCopy(senhas);
          
          int n = 0;
          int nMax = (int)(TEMPO_FECHO * 60/TEMPO_MEDIO_ATENDIMENTO) - (int)(TEMPO_ABERTURA * 60/TEMPO_MEDIO_ATENDIMENTO);
          
          for(int i=0;i<nMax;i++){
              out.put(i, new HashMap<>());
          }
          
          while( n < nMax){
              Iterator<Senha> it = aux.iterator();
              while(it.hasNext()){
                  Senha s = it.next();
                  if(getReparticaoDeCidadao(getCidadaoByNumeroContribuinte(s.getNumeroContribuinte())).getNumeroReparticao() != r.getNumeroReparticao())
                      break;
                  if(isNextInLine(s,aux) && !out.get(n).containsKey(s.getCodigo())){
                      if( out.get(n).containsValue(s.getNumeroContribuinte()) ){
                        int i=0;
                        while( n+i < nMax){
                            if( !out.get( n+i ).containsValue( s.getNumeroContribuinte()) ){
                                out.get( n+i ).put(s.getCodigo(), s.getNumeroContribuinte());
                                break;
                            }
                            i++;
                        }
                      }else{
                        out.get(n).put(s.getCodigo(), s.getNumeroContribuinte());
                      }
                      it.remove();
                  }
              }
              n++;
          }
          
          Map<String,HashMap<Character,String>> res = new HashMap<>();
          for(int i=0; i < nMax; i++){
              int min = ((int)((TEMPO_ABERTURA - (int)TEMPO_ABERTURA)*60) + (i)*TEMPO_MEDIO_ATENDIMENTO) % 60;
              int horas = (int)TEMPO_ABERTURA + (30 + (i)*TEMPO_MEDIO_ATENDIMENTO) /60;
              res.put(horas + ":" + min, out.get(i));
          }
          return res;
    }

    private Reparticao getReparticaoByNumeroReparticao(int numeroReparticao) {
        for (Reparticao r : reparticoes) {
            if (r.getNumeroReparticao() == numeroReparticao) {
                return r;
            }
        }
        return null;
    }

    private Cidadao getCidadaoByNumeroContribuinte(String numeroContribuinte) {
        for (Cidadao c : cidadaos) {
            if (c.getNumeroContribuinte().equals(numeroContribuinte)) {
                return c;
            }
        }
        return null;
    }
    
    public List<Character> getServicosMaiorProcura(){
        HashMap<Character,Integer> freq = new HashMap<>();
        int max = 0;
        for(Senha s : senhas){
            Character servico = s.getCodigo();
            freq.put(servico, freq.containsKey(servico)? freq.get(servico) + 1 : 1 );
        
            if(freq.get(servico) > max){
                max = freq.get(servico);
            }
        }
        
        List<Character> out = new ArrayList<>();
        for (Character servico : freq.keySet()) {
            if(freq.get(servico) == max)
                out.add(servico);
        }
        
        return out;
    }
    
    public void abandonarFilas(Cidadao c){
        Iterator<Senha> it = senhas.iterator();
        while(it.hasNext()){
            if(it.next().getNumeroContribuinte().equals(c.getNumeroContribuinte()))
                it.remove();
        }
    }
}
