package modal;

import java.util.Objects;

/**
 *
 * @author Anna
 */


// teste
public class Cidadao {
    
    private String numeroContribuinte;
    private String nome;
    private String endrecoCorreio;
    private String codigoPostal;
    private int numeroReparticao;

    public Cidadao(String numeroContribuinte, String nome, String endrecoCorreio, String codigoPostal, int numeroReparticao) {
        this.numeroContribuinte = numeroContribuinte;
        this.nome = nome;
        this.endrecoCorreio = endrecoCorreio;
        this.codigoPostal = codigoPostal;
        this.numeroReparticao = numeroReparticao;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public int getNumeroReparticao() {
        return numeroReparticao;
    }

    public String getNumeroContribuinte() {
        return numeroContribuinte;
    }
    
    public void setNumeroReparticao(int numeroReparticao) {
        this.numeroReparticao = numeroReparticao;
    }
    
    @Override
    public int hashCode() {        
        return this.numeroContribuinte.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Cidadao c = (Cidadao) obj;
        return c.codigoPostal.equals(this.codigoPostal) &&
                c.endrecoCorreio.equals(this.endrecoCorreio) &&
                c.numeroContribuinte.equals(this.numeroContribuinte) &&
                c.nome.equals(this.nome) &&
                c.numeroReparticao == this.numeroReparticao;
    }
    
    
}
