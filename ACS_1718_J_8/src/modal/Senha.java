package modal;

public class Senha {

    private int numero;
    private Character codigo;
    private String numeroContribuinte;

    public Senha(int numero, Character codigo, String numeroContribuinte) {
        this.numero = numero;
        this.codigo = codigo;
        this.numeroContribuinte = numeroContribuinte;
    }

    public int getNumero() {
        return numero;
    }

    public Character getCodigo() {
        return codigo;
    }

    public String getNumeroContribuinte() {
        return numeroContribuinte;
    }
    
    @Override
    public int hashCode() {
        return this.numero *
                this.codigo.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Senha s = (Senha) obj;
        
        return this.numero == s.numero && this.codigo == s.codigo && this.numeroContribuinte.equals(s.numeroContribuinte);
    }
    
    
}
