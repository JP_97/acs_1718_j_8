package modal;

import utils.DoublyLinkedList;
import utils.DoublyLinkedListUtils;

public class Reparticao {

    private String cidade;
    private String codigoPostal;
    private DoublyLinkedList<Character> servicos;
    private int numeroReparticao;

    public Reparticao(String cidade, String codigoPostal, int numeroReparticao) {
        this.cidade = cidade;
        this.codigoPostal = codigoPostal;
        this.numeroReparticao = numeroReparticao;
        this.servicos = new DoublyLinkedList<>();
    }
    
    public void addServico(Character servico){
        servicos.addLast(servico);
    }
    
    public void addCidadao(Character servico){
        servicos.addLast(servico);
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public int getNumeroReparticao() {
        return numeroReparticao;
    }

    public String getCidade() {
        return cidade;
    }
    
    public boolean contemServico(Character servico){
        for(Character s : servicos){
            if(s == servico)
                return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.numeroReparticao;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Reparticao r = (Reparticao) obj;
        
        DoublyLinkedListUtils<Character> listUtil1 = new DoublyLinkedListUtils<>();
        
        return r.cidade.equals(cidade) && r.codigoPostal.equals(codigoPostal)
                && r.numeroReparticao == this.numeroReparticao &&
                listUtil1.compareLists(r.servicos, servicos);
    }
    
    
    
}
