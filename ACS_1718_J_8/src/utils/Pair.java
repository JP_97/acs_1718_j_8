package utils;

import java.util.Objects;

public class Pair<E,T> {
    
    private E first;
    private T second;

    public Pair(E first, T second) {
        this.first = first;
        this.second = second;
    }

    public E getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }

    @Override
    public int hashCode() {
        return first.hashCode() + second.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair<E, T> pair = (Pair<E, T>) obj;
        return pair.first.equals(this.first) && pair.second.equals(this.second);
    }
    
    
    
}
