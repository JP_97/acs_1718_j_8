package utils;

import java.util.Iterator;
import java.util.List;
import modal.Reparticao;

public class DoublyLinkedListUtils<E> {

    

        public DoublyLinkedListUtils() {
        }
    
       public boolean compareLists(DoublyLinkedList<E> l1, DoublyLinkedList<E> l2){
            if(l1.size() != l2.size()) return false;
            Iterator<E> it1 = l1.iterator();
            Iterator<E> it2 = l2.iterator();
            while(it1.hasNext() && it2.hasNext()){
                E e = it1.next();
                E e2 = it2.next();
                if(!e.equals(e2)) 
                    return false;
            }
            return true;
       }
        
       public boolean remove(DoublyLinkedList<E> l, E e) {
           Iterator<E> it = l.iterator();
           while(it.hasNext()){
               if(it.next().equals(e)){
                   it.remove();
                   return true;
               }
           }
           return false;
       }
       
       public DoublyLinkedList<E> getCopy(DoublyLinkedList<E> l){
           DoublyLinkedList<E> c = new DoublyLinkedList<>();
           for(E e : l){
               c.addLast(e);
           }
           return c;
       }
}
