package modal;

import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.DoublyLinkedList;
import utils.DoublyLinkedListUtils;
import utils.Pair;

public class GestorAtendimentoServicosPublicosTest {
    
    public GestorAtendimentoServicosPublicosTest() {
    }
    
    /**
     * Test of lerReparticoesDeFicheiro method, of class GestorAtendimentoServicosPublicos.
     */
    @Test
    public void testLerReparticoesDeFicheiro() throws Exception {
        System.out.println("lerReparticoesDeFicheiro");
        String ficheiro = "fx_teste_reparticoes.txt";
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.lerReparticoesDeFicheiro(ficheiro);
        DoublyLinkedList<Reparticao> reparticoesExpected = new DoublyLinkedList<>();
        Reparticao r1 = new Reparticao("Porto", "4200", 1234);
        r1.addServico('A');
        r1.addServico('B');
        r1.addServico('C');
        r1.addServico('D');
        Reparticao r2 = new Reparticao("Maia", "4470", 1235);
        r2.addServico('A');
        r2.addServico('B');
        reparticoesExpected.addLast(r1);
        reparticoesExpected.addLast(r2);
        
        DoublyLinkedListUtils listUtils = new DoublyLinkedListUtils();
        assertTrue("List arent equal", listUtils.compareLists(reparticoesExpected, instance.getReparticoes()));
    }

    /**
     * Test of lerSenhasDeFicheiro method, of class GestorAtendimentoServicosPublicos.
     */
    @Test
    public void testLerSenhasDeFicheiro() throws Exception {
        System.out.println("lerSenhasDeFicheiro");
        String ficheiro = "fx_teste_senhas.txt";
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.lerReparticoesDeFicheiro("fx_teste_reparticoes.txt");
        instance.lerCidadaosDeFicheiro("fx_teste_cidadaos.txt");
        instance.lerSenhasDeFicheiro(ficheiro);
        DoublyLinkedList<Senha> senhasExpected = new DoublyLinkedList<>();
        Senha s1 = new Senha(1, 'A', "111222333");
        Senha s2 = new Senha(1, 'C', "111222333");
        Senha s3 = new Senha(1, 'B', "111222333");
        Senha s4 = new Senha(2, 'C', "111222333");
        Senha s5 = new Senha(2, 'B', "333444555");
        Senha s6 = new Senha(2, 'A', "222333444");
        Senha s7 = new Senha(3, 'A', "333444555");
        senhasExpected.addLast(s1);
        senhasExpected.addLast(s2);
        senhasExpected.addLast(s3);
        senhasExpected.addLast(s4);
        senhasExpected.addLast(s5);
        senhasExpected.addLast(s6);
        senhasExpected.addLast(s7);

        
        DoublyLinkedListUtils listUtils = new DoublyLinkedListUtils();
        assertTrue("Lists arent equal", listUtils.compareLists(senhasExpected, instance.getSenhas()));
    }

    @Test
    public void testLerCidadaosDeFicheiro() throws Exception {
        System.out.println("lerCidadaosDeFicheiro");
        String ficheiro = "fx_teste_cidadaos.txt";
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.lerReparticoesDeFicheiro("fx_teste_reparticoes.txt");
        instance.lerCidadaosDeFicheiro(ficheiro);

        DoublyLinkedList<Cidadao> expected = new DoublyLinkedList<>();
        Cidadao c1 = new Cidadao("111222333","Ana","ana@gmail.com","4200-072",1234);
        Cidadao c2 = new Cidadao("222333444", "Berta",  "berta@gmail.com","4200-071", 1234);
        Cidadao c3 = new Cidadao("333444555", "Manuel",  "manuel@gmail.com","4715-375", 1234);
        expected.addLast(c1);
        expected.addLast(c2);
        expected.addLast(c3);
        
        DoublyLinkedListUtils listUtils = new DoublyLinkedListUtils();
        assertTrue("Lists arent equal", listUtils.compareLists(expected, instance.getCidadaos()));
    }

    @Test
    public void testGetCidadaosAfectosACadaReparticao() throws Exception{
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.lerReparticoesDeFicheiro("fx_teste_reparticoes.txt");
        instance.lerCidadaosDeFicheiro("fx_teste_cidadaos.txt");
        
        Map<Pair<Integer,String>,List<String>> actual = instance.getCidadaosAfectosACadaReparticao();
        
        Map<Pair<Integer,String>,List<String>> expected = new HashMap<Pair<Integer,String>,List<String>>();
        List<String> l1 = new ArrayList<>();
        l1.add("111222333");
        l1.add("222333444");
        l1.add("333444555");
        List<String> l2 = new ArrayList<>();
        expected.put(new Pair<>(1234,"Porto"), l1);
        expected.put(new Pair<>(1235,"Maia"), l2);
    
        assertTrue("Maps should be equal",expected.equals(actual));
    }
    
    /**
     * Test of addReparticao method, of class GestorAtendimentoServicosPublicos.
     */
    @Test
    public void testAddReparticao() throws Exception {
        System.out.println("addReparticao");
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        Reparticao r = new Reparticao("Porto","4200",1111);
        Reparticao r2 = new Reparticao("Porto","4200",1111);
        instance.addReparticao(r);
        try{
            instance.addReparticao(r2);
        } catch(Exception e){
            return;
        }
        assertTrue("Should have thrown exception",false);
    }

    /**
     * Test of removeReparticao method, of class GestorAtendimentoServicosPublicos.
     */
    @Test
    public void testRemoveReparticao() throws Exception{
        System.out.println("removeReparticao");
        Reparticao r1 = new Reparticao("Porto", "4200", 1234);
        Reparticao r2 = new Reparticao("Porto", "4000", 1235);
        Reparticao r3 = new Reparticao("Porto", "4300", 1236);
        Cidadao c = new Cidadao("1234", "Joao", "Joao@sapo.pt", "4200-700", 1234);
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.addReparticao(r1);
        instance.addReparticao(r2);
        instance.addReparticao(r3);
        instance.getCidadaos().addLast(c);
        
        instance.removeReparticao(r1);
        
        DoublyLinkedList<Reparticao> expectedReparticoes = new DoublyLinkedList<>();
        expectedReparticoes.addLast(r2);
        expectedReparticoes.addLast(r3);
        
        DoublyLinkedListUtils<Reparticao> listUtils = new DoublyLinkedListUtils();
        assertTrue("List arent equal",listUtils.compareLists(expectedReparticoes, instance.getReparticoes()));
        
        assertTrue("Cidadao não foi movido para a repartição mais proxima",c.getNumeroReparticao() == 1236);
    }
    
    @Test
    public void getUtilizacaoReparticaoTest() throws Exception{
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.lerReparticoesDeFicheiro("fx_teste_reparticoes.txt");
        instance.lerCidadaosDeFicheiro("fx_teste_cidadaos.txt");
        instance.lerSenhasDeFicheiro("fx_teste_senhas.txt");
        Map<String, HashMap<Character,String>> actual = instance.getUtilizacaoReparticao(instance.getReparticoes().first());
        
        Map<String,HashMap<Character,String>> res = new HashMap<>();
        HashMap<Character,String> h1 = new HashMap<>();
        h1.put('A', "111222333");
        h1.put('B', "333444555");
        HashMap<Character,String> h2 = new HashMap<>();
        h2.put('C', "111222333");
        h2.put('A', "222333444");
        HashMap<Character,String> h3 = new HashMap<>();
        h3.put('B', "111222333");
        h3.put('A', "333444555");
        HashMap<Character,String> h4 = new HashMap<>();
        h4.put('C', "111222333");
        res.put("9:30", h1);
        res.put("9:40", h2);
        res.put("9:50", h3);
        res.put("10:0", h4);
        
        for(String time : res.keySet()){
            assertTrue("Maps should be equal",res.get(time).equals(actual.get(time)));
        }
    }

    
    @Test
    public void abandonarFilasTest() throws Exception{
        Reparticao r1= new Reparticao("Porto", "4000-200",1);
        r1.addServico('A');
        r1.addServico('B');
        Cidadao c1 = new Cidadao("250666122", "Joao Mario", "joaomario@gmail.com", "4000-102", 1);
        Cidadao c2 = new Cidadao("250666124", "Joao Mario", "joaomario@gmail.com", "4000-102", 1);
        Senha s1 = new Senha(3,'A',"250666122");
        Senha s2 = new Senha(4,'B',"250666124");
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.addReparticao(r1);
        instance.addCidadao(c1);
        instance.addCidadao(c2);
        instance.addSenha(s1);
        instance.addSenha(s2);
        
        instance.abandonarFilas(c1);
        
        DoublyLinkedListUtils<Senha> lu = new DoublyLinkedListUtils<>();
        
        DoublyLinkedList<Senha> actual = instance.getSenhas();
        DoublyLinkedList<Senha> expected = new DoublyLinkedList<>();
        expected.addLast(s2);
        
        assertTrue("List should be equal",lu.compareLists(actual, expected));
    }
    
    
    @Test
    public void getServicosMaiorProcuraTest() throws Exception{
        
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
        instance.lerReparticoesDeFicheiro("fx_teste_reparticoes.txt");
        instance.lerCidadaosDeFicheiro("fx_teste_cidadaos.txt");
        instance.lerSenhasDeFicheiro("fx_teste_senhas.txt");
        instance.getUtilizacaoReparticao(instance.getReparticoes().first());
        
        
        List<Character> actual = instance.getServicosMaiorProcura();
        List<Character> expected = new ArrayList<>();
        expected.add('A');
        
        assertTrue("Lists should be equal",expected.size() == actual.size());
        
        Iterator<Character> itActual = actual.iterator();
        Iterator<Character> itExpected = expected.iterator();
        while( itActual.hasNext() ){
            assertTrue("Lists should be equal",itActual.next() == itExpected.next());
        }
        
    }
    
    /**
     *
     */
    
    @Test
    public void testAddCidadao() throws Exception{
        System.out.println("addCidadao");
        GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
         Reparticao r = new Reparticao("Porto","4200",1);
         instance.addReparticao(r);
        Cidadao c1 = new Cidadao("250666122", "Joao Mario", "joaomario@gmail.com", "4000-102", 142);
        Cidadao c2 = new Cidadao("250666124", "Joao Manuel", "joaomario@gmail.com", "4000-102", 1);
        instance.addCidadao(c2); 
         
        
        try{
            instance.addCidadao(c2);
        } catch(Exception e){
            try{
                instance.addCidadao(c1);
            } catch(Exception ex){
                return;
            }
        }
        assertTrue("Should have thrown exception",false);
        
    }
 
    
    
    @Test
    
     public void testAddSenha() throws Exception{
         System.out.println("addSenha");
         GestorAtendimentoServicosPublicos instance = new GestorAtendimentoServicosPublicos();
         Senha s=new Senha(1,'a',"250222111");
         Reparticao r = new Reparticao("cidade", "codigoPostal", 0);
         Cidadao c = new Cidadao("250222222", "Joao", "Joao@gmail.com", "4000", 0);
         try{
           instance.addSenha(s); 
             
             
         } catch(Exception e){
         try{
                instance.addReparticao(r);
                instance.addCidadao(c);
                instance.addSenha(s);
            } catch(Exception ex){
                return;
            }
        }
        assertTrue("Should have thrown exception",false);
        
    }
     
     
  
     
     
     
 
    
}
