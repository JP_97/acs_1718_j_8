package utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class DoublyLinkedListUtilsTest {
    
    public DoublyLinkedListUtilsTest() {
    }

    /**
     * Test of compareLists method, of class ListUtils.
     */
    @Test
    public void testCompareLists() {
        System.out.println("compareLists");
        DoublyLinkedListUtils instance = new DoublyLinkedListUtils();
        DoublyLinkedList<String> list1 = new DoublyLinkedList<>();
        list1.addLast("joaquim");
        list1.addLast("josefino");
        list1.addLast("geraldino");
        DoublyLinkedList<String> list2 = new DoublyLinkedList<>();
        list2.addLast("joaquim");
        list2.addLast("josefino");
        list2.addLast("geraldino");
        
        assertTrue("Lists are equal!",instance.compareLists(list1, list2));
    }
    
    
    @Test
    public void getCopyTest(){
        DoublyLinkedListUtils<Integer> instance = new DoublyLinkedListUtils();
        DoublyLinkedList<Integer> expected = new DoublyLinkedList<>();
        expected.addLast(1);
        expected.addLast(2);
        expected.addLast(3);
        expected.addLast(4);
        expected.addLast(5);
        DoublyLinkedList<Integer> actual = instance.getCopy(expected);
        
        assertTrue("Lists should be equal", instance.compareLists(actual, expected));
    }
      ;
        
    
    @Test
    
    public void removeTest(){
        DoublyLinkedListUtils<Integer> instance = new DoublyLinkedListUtils();
        
        DoublyLinkedList<Integer> actual = new DoublyLinkedList<>();
        actual.addLast(1);
        actual.addLast(2);
        actual.addLast(3);
        actual.addLast(4);
        actual.addLast(5);
        
        instance.remove(actual, 4);
        
        DoublyLinkedList<Integer> expected = new DoublyLinkedList<>();
        expected.addLast(1);
        expected.addLast(2);
        expected.addLast(3);
        expected.addLast(5);
        
         assertTrue("Lists should be equal", instance.compareLists(actual, expected));
        
    }
        
        
    
    
    
    
    
}
